from os import environ
from dotenv import load_dotenv
from os.path import abspath
# #################################
load_dotenv()
# **********************************
class Congif:
    SERVER = "localhost:80"
    DEBUG = True

    # DATABASE_PATH = "app/database/contact_book.db"
    DB_TOKEN = environ.get("SECRET_KEY", "")
    FLASK_ENV = environ.get("FLASK_ENV")
    FLASK_APP = "./wsgi.py"  # Para Encriptar la DB
    ENCRYPT_DB = True

    TEMPLATE_FOLDER = abspath('src/plantillas') 
    STATIC_FOLDER = abspath('src/media') 

    print(STATIC_FOLDER)

    # Configuracion de la base de datos
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:root@host:port/genesis'
		  
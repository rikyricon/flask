from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Congif
from .blog.rutas.blog import blog
from .admin.rutas.admin import admin

db = SQLAlchemy()
migrate = Migrate()

def app_genesis():
    gen = Flask(__name__, instance_relative_config=True, static_folder=Congif.STATIC_FOLDER, template_folder=Congif.TEMPLATE_FOLDER)
    gen.config.from_object(Congif)

    with gen.app_context():

        db.init_app(gen)
        migrate.init_app(gen, db)

        # Rutas del sitio
        gen.add_url_rule(blog['index'], view_func=blog['blog_controller'])
        gen.add_url_rule(admin['index'], view_func=admin['admin_controller'])

        return gen

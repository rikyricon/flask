from flask.views import MethodView
from flask import render_template, url_for, redirect

class IndexController(MethodView):

    def get(self):
        return render_template('blog/index.html')
        # return 'Hola'
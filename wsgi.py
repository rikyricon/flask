from flask import Config
from src import app_genesis


app = app_genesis()

if __name__ == "__main__":
    app.run(host="localhost", port=80)